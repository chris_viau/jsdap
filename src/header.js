/*
 * jsDAP 4.1.2, a JavaScript OPeNDAP client.
 *
 * You can find the uncompressed source at:
 *
 *   https://bitbucket.org/jetfuse/jsdap
 *
 * Copyright (c) 2007--2009 Roberto De Almeida
 */
